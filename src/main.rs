extern crate game_kernel;

use game_kernel::{game_kernel_main};

fn main() {
    println!("Hello, world!");
    //game_kernel_main();
    let mut sbsts = game_kernel::game_kernel::subsystems::init().unwrap();
    {
        sbsts.renderer.renderer.as_ref().unwrap().borrow_mut();
    }
    sbsts.main_loop();
    //loop{}
}
